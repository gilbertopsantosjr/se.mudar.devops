
aws sns create-topic --name local-eventing-middleware-users-topic.fifo --attributes "FifoTopic=true,ContentBasedDeduplication=true"

aws sqs create-queue --queue-name local-users-events.fifo --attributes "maxReceiveCount=2,FifoQueue=true,ContentBasedDeduplication=true,deadLetterTargetArn=arn:aws:sqs:us-east-1:queue:TESTDLQ.fifo"

aws sns subscribe --topic-arn arn:aws:sns:us-east-1:000000000000:local-eventing-middleware-users-topic.fifo --protocol sqs --notification-endpoint arn:aws:sqs:us-east-1:000000000000:local-users-events.fifo.fifo