
docker stats

docker network create --driver bridge mudar-network
apt-get update && apt-get install -y iputils-ping

docker volume create runtime


docker run -v $HOME/runtime/logs:/data -d --name configserver -p 8888:8888 --memory="1g" --cpus="0.5" --memory-reservation="750m" --network=mudar-network --hostname=configserver -e SPRING_PROFILES_ACTIVE=dev -e LOG_PATH=$HOME/runtime/logs mudarsecontainerregistry.azurecr.io/mudar-se/infra/configserver:latest

docker run -v $HOME/runtime/logs:/data -d --name dashboard -p 8961:8961 --memory="1g" --cpus="0.5" --memory-reservation="750m" --network=mudar-network --hostname=dashboard -e SPRING_PROFILES_ACTIVE=local mudarsecontainerregistry.azurecr.io/mudar-se/infra/dashboard:latest

docker run -v $HOME/runtime/logs:/data -d --name discovery -p 8761:8761 --memory="1g" --cpus="0.5" --memory-reservation="750m" --network=mudar-network --hostname=discovery -e SPRING_PROFILES_ACTIVE=local mudarsecontainerregistry.azurecr.io/mudar-se/infra/discovery:latest

docker run -v $HOME/runtime/logs:/data -d --name gateway -p 5555:5555 --memory="1g" --cpus="0.5" --memory-reservation="750m" --network=mudar-network --hostname=gateway -e SPRING_PROFILES_ACTIVE=local mudarsecontainerregistry.azurecr.io/mudar-se/infra/gateway:latest

docker run -v $HOME/runtime/logs:/data -d --name auth -p 8088:8088 --memory="1g" --cpus="0.5" --memory-reservation="750m" --network=mudar-network --hostname=auth -e SPRING_PROFILES_ACTIVE=local mudarsecontainerregistry.azurecr.io/mudar-se/infra/auth:latest



docker exec -it stack_dns_1 hostname
docker-compose -p kafka -f docker-infra-dev.yml up

To delete all containers including its volumes use,
docker rm -vf '$(docker ps -a -q)'
docker rm $(docker ps -aq)

To delete all network
docker network rm $(docker network ls -q)

To delete all the images,
docker rmi -f $(docker images -a -q)

docker pull gcr.io/mudar-se/infra/auth:latest



