-- create our main user for local dev
CREATE USER IF NOT EXISTS 'localdev'@'%' IDENTIFIED BY 'p123456s';

-- user service and auth
CREATE DATABASE IF NOT EXISTS `users` DEFAULT CHARACTER SET utf8 COLLATE utf8_bin;
GRANT ALL PRIVILEGES ON `users`.* TO 'localdev'@'%'  WITH GRANT OPTION;

-- track my money
CREATE DATABASE IF NOT EXISTS `track_my_money` DEFAULT CHARACTER SET utf8 COLLATE utf8_bin;
GRANT ALL PRIVILEGES ON `track_my_money`.* TO 'localdev'@'%'  WITH GRANT OPTION;


FLUSH PRIVILEGES;